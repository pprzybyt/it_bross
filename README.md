# itbross

initial steps:

- git clone https://github.com/pprzybyt/itbross.git
- pyton install app_itbross-0.0-py3-none-any.whl
- cd app_itbross-0.0 
- pip install -e .

If you don't have postgresql installed please follow:
  - windows: https://www.openscg.com/bigsql/postgresql/installers.jsp/ and version 9.5 (add bin dir of psql into $path variable - e.g. C:\PostgreSQL\pg95\bin)
  
  - dembian : sudo apt-get update; sudo apt-get install postgresql postgresql-contrib

Then prepare project database:

      c:\> psql -U postgres or $ psql -U postgres
      
      postgres=# CREATE DATABASE pyramid;
      
      postgres=# CREATE USER admin WITH PASSWORD 'admin';
      
      postgres=# GRANT ALL PRIVILEGES ON pyramid to admin;
  
  This step is required beacause postgresql suports ARRAY and JSON types used in db.
  
  Then move to: .../app_itbross-0.0/ and type:
  - initialize_app_itbross_db development.ini
  - pserve development.ini --reload
  
  output should be displayed at: localhost:6543 in your browser

If you have any problems please contact me via pprzybyt@gmail.com


