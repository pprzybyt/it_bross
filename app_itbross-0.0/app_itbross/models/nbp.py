import datetime #<- will be used to set default dates on models

from .meta import Base

from sqlalchemy import (
    Column,
    Integer,
    Unicode,     #<- will provide Unicode field
    UnicodeText, #<- will provide Unicode text field
    DateTime,    #<- time abstraction field
    Date,
    Text,
)

from sqlalchemy.dialects import postgresql
from sqlalchemy import cast


class CastingArray(postgresql.ARRAY):
    def bind_expression(self, bindvalue):
        return cast(bindvalue, self)


class Nbp(Base):
    __tablename__ = 'nbp'
    id = Column(Integer, primary_key=True)
    no = Column(Unicode(255), unique=True, nullable=False)
    effectiveDate = Column(Date)
    rates = Column(CastingArray(postgresql.JSON))

