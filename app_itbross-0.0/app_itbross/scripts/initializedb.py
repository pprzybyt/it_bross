import os
import sys
import transaction
import datetime

from pyramid.paster import (
    get_appsettings,
    setup_logging,
    )

from pyramid.scripts.common import parse_vars

from ..models.meta import Base
from ..models import (
    get_engine,
    get_session_factory,
    get_tm_session,
    )

from ..models import Nbp


def usage(argv):
    cmd = os.path.basename(argv[0])
    print('usage: %s <config_uri> [var=value]\n'
          '(example: "%s development.ini")' % (cmd, cmd))
    sys.exit(1)


def main(argv=sys.argv):
    if len(argv) < 2:
        usage(argv)
    config_uri = argv[1]
    options = parse_vars(argv[2:])
    setup_logging(config_uri)
    settings = get_appsettings(config_uri, options=options)

    engine = get_engine(settings)
    Base.metadata.create_all(engine)
    # Base.metadata.drop_all(engine)
    session_factory = get_session_factory(engine)

    with transaction.manager:
        dbsession = get_tm_session(session_factory, transaction.manager)
        nbp = Nbp(no=u'138/A/NBP/2018', effectiveDate=datetime.date(2018, 7, 12), rates=[{"currency":"bat (Tajlandia)","code":"THB","mid":0.1106},{"currency":"dolar amerykański","code":"USD","mid":3.6828},{"currency":"dolar australijski","code":"AUD","mid":2.7365},{"currency":"dolar Hongkongu","code":"HKD","mid":0.4692},{"currency":"dolar kanadyjski","code":"CAD","mid":2.8007},{"currency":"dolar nowozelandzki","code":"NZD","mid":2.4951},{"currency":"dolar singapurski","code":"SGD","mid":2.7032},{"currency":"euro","code":"EUR","mid":4.3076},{"currency":"forint (Węgry)","code":"HUF","mid":0.013372},{"currency":"frank szwajcarski","code":"CHF","mid":3.6816},{"currency":"funt szterling","code":"GBP","mid":4.8811},{"currency":"hrywna (Ukraina)","code":"UAH","mid":0.1405},{"currency":"jen (Japonia)","code":"JPY","mid":0.032741},{"currency":"korona czeska","code":"CZK","mid":0.1665},{"currency":"korona duńska","code":"DKK","mid":0.5778},{"currency":"korona islandzka","code":"ISK","mid":0.034406},{"currency":"korona norweska","code":"NOK","mid":0.4544},{"currency":"korona szwedzka","code":"SEK","mid":0.4161},{"currency":"kuna (Chorwacja)","code":"HRK","mid":0.5825},{"currency":"lej rumuński","code":"RON","mid":0.9258},{"currency":"lew (Bułgaria)","code":"BGN","mid":2.2025},{"currency":"lira turecka","code":"TRY","mid":0.7607},{"currency":"nowy izraelski szekel","code":"ILS","mid":1.0124},{"currency":"peso chilijskie","code":"CLP","mid":0.005662},{"currency":"peso meksykańskie","code":"MXN","mid":0.1956},{"currency":"piso filipińskie","code":"PHP","mid":0.0689},{"currency":"rand (Republika Południowej Afryki)","code":"ZAR","mid":0.2782},{"currency":"real (Brazylia)","code":"BRL","mid":0.9566},{"currency":"ringgit (Malezja)","code":"MYR","mid":0.9098},{"currency":"rubel rosyjski","code":"RUB","mid":0.0593},{"currency":"rupia indonezyjska","code":"IDR","mid":0.0002562},{"currency":"rupia indyjska","code":"INR","mid":0.053694},{"currency":"won południowokoreański","code":"KRW","mid":0.003267},{"currency":"yuan renminbi (Chiny)","code":"CNY","mid":0.5510},{"currency":"SDR (MFW)","code":"XDR","mid":5.1870}])
        dbsession.add(nbp)
