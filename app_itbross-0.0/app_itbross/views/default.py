from pyramid.response import Response
from pyramid.view import view_config
import datetime
from pyramid.httpexceptions import HTTPFound
import json
import urllib.request as request
from sqlalchemy.exc import DBAPIError, IntegrityError
import sqlalchemy
# from psycopg2 import IntegrityError
from ..models import Nbp
import ast

import logging
log = logging.getLogger(__name__)
json_payload = json.dumps({'a': 1})
headers = {'Content-Type': 'application/json; charset=utf-8'}


class Views(object):
    def __init__(self, request):
        self.request = request

    @view_config(route_name='home', renderer='../templates/mytemplate.jinja2')
    def my_view(self):
        try:
            one = self.request.dbsession.query(Nbp).order_by(Nbp.effectiveDate.desc()).first()
        except DBAPIError:
            return Response(db_err_msg, content_type='text/plain', status=500)
        return {'one': one, 'project': 'app_itbross'}

    @view_config(route_name='update', renderer='../templates/mytemplate.jinja2')
    def update(self):
        resp = request.urlopen('http://api.nbp.pl/api/exchangerates/tables/A/?format=json')
        json_data = ast.literal_eval(resp.read().decode("utf-8"))
        json_data = json.loads(json.dumps(json_data))[0]

        try:
            nbp = Nbp(no=json_data['no'], effectiveDate=json_data['effectiveDate'], rates=json_data['rates'])
            self.request.dbsession.merge(nbp)
            self.request.dbsession.flush()
        except sqlalchemy.exc.IntegrityError as e:
            log.debug(e)
            self.request.dbsession.rollback()
            one = self.request.dbsession.query(Nbp).order_by(Nbp.effectiveDate.desc()).first()
            return {'one': one, 'project': 'app_itbross', 'error': '(up to date)'}

        url = self.request.route_url('home')
        return HTTPFound(url)


db_err_msg = """\
Pyramid is having a problem using your SQL database.  The problem
might be caused by one of the following things:

1.  You may need to run the "initialize_app_itbross_db" script
    to initialize your database tables.  Check your virtual
    environment's "bin" directory for this script and try to run it.

2.  Your database server may not be running.  Check that the
    database server referred to by the "sqlalchemy.url" setting in
    your "development.ini" file is running.

After you fix the problem, please restart the Pyramid application to
try it again.
"""
