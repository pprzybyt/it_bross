app_itbross README
==================

Getting Started
---------------

- cd <directory containing this file>

- $VENV/bin/pip install -e .

- $VENV/venv/bin/initialize_app_itbross_db development.ini

- $VENV/bin/pserve development.ini

